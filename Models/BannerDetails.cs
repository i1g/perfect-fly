﻿namespace PerfectFly.Models
{
    public class BannerDetails
    {
        public string DepartureAirportName { get; set; }

        public string ArrivalAirportName { get; set; }

        public string ArrivalCityName { get; set; }

        public string MonthDisplayName { get; set; }

        public decimal Temperature { get; set; }

        public bool IsCelsius { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }
    }
}
