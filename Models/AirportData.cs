﻿namespace PerfectFly.Models
{
    public class AirportData
    {
        public string AirportСode { get; set; }

        public string AirportName { get; set; }

        public string CityName { get; set; }

        public string CityСode { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }
    }
}
