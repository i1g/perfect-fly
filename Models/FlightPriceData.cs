﻿using System;

namespace PerfectFly.Models
{
    public class FlightPriceData
    {
        public decimal Price { get; set; }

        public string Currency { get; set; }
    }
}
