﻿using System;

namespace PerfectFly.Models
{
    public class TemperatureData
    {
        public decimal AverageTemperature { get; set; }

        public bool IsCelsius { get; set; }
    }
}
