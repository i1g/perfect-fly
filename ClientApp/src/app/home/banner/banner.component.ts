import { Component, Input, OnInit } from '@angular/core';
import { BannerDetailsService } from './../../banner-details.service';
import { BannerDetails } from './../../banner-details.model';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  @Input() departureAirportCode: string; // For example, OSL
  @Input() arrivalAirportCode: string; // For example, JFK
  @Input() language: string; // For example, en-US
  // @Input() month?: number; // In theory, it should be daterange (DateRangeFrom, DateRangeTo):
  @Input() dateFrom?: string;
  @Input() dateTo?: string;

  bannerDetails: BannerDetails;
  imgUrl: string; // Random image url to show in the banner

  // Can be moved to separate file with Constants, if needed
  private readonly WARM_TEMPERATURE = 30; // In Celsius

  constructor(private bannerDetailsService: BannerDetailsService) { }

  ngOnInit() {
    this.getBannerDetails();
  }

  getBannerDetails() {
    this.bannerDetailsService
      .getBannerDetails(this.departureAirportCode, this.arrivalAirportCode, this.language, this.dateFrom, this.dateTo)
      .subscribe((bannerDetails: BannerDetails) => {
        this.bannerDetails = bannerDetails;
        this.chooseRandomImage(bannerDetails);
        console.log(bannerDetails);
      }, error => {
        // Maybe error-handling / showing 'default' banner
        console.log(error)
      });
  }

  chooseRandomImage(bannerDetails: BannerDetails) {
    // Choose random image, depending on temperature (special image/-s for warm temperature)

    // This should be stored maybe in separate file (and/or can pre-fix images with city codes)
    const availableBanners = ['NY_1.png', 'NY_2.png'];
    const availableSpecialBanners = ['summer.png'];

    if (this.isWarmTemperature(bannerDetails.temperature, bannerDetails.isCelsius)) {
      this.imgUrl = availableSpecialBanners[Math.floor(Math.random() * availableSpecialBanners.length)];
    } else {
      this.imgUrl = availableBanners[Math.floor(Math.random() * availableBanners.length)];
    }
  }

  isWarmTemperature(temperature: number, isCelsius: boolean): boolean {
    // If average temperature is higher than 20.0° Celsius, then it is considered as 'Warm'

    // If temperature is in Celsius scale, just compare with 20 (WARM_TEMPERATURE)
    // Otherwise, convert temperature from F to C, and compare with WARM_TEMPERATURE
    return isCelsius ? temperature > this.WARM_TEMPERATURE : ((temperature - 32) * 5 / 9) > this.WARM_TEMPERATURE;
  }
}
