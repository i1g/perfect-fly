export interface BannerDetails {
  departureAirportName: string;
  arrivalAirportName: string;
  arrivalCityName: string;
  monthDisplayName: string;
  temperature: number;
  isCelsius: boolean; // Or could be enum
  price: number;
  currency: string; // Maybe not needed if can use language/locale and CurrencyPipe
}