import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BannerDetails } from './banner-details.model';

@Injectable({
  providedIn: 'root'
})
export class BannerDetailsService {

  private url = 'api/bannerdetails';

  private defaultBannerDetails: BannerDetails = {
    departureAirportName: 'Oslo lufthavn',
    arrivalAirportName: 'John F. Kennedy International Airport',
    arrivalCityName: 'New York City',
    monthDisplayName: 'June',
    temperature: 80,
    isCelsius: false,
    price: 100,
    currency: '$' // or maybe 'USD'
  };

  constructor(private http: HttpClient) { }

  getBannerDetails(departureAirportCode: string, arrivalAirportCode: string, language: string, dateFrom?: string, dateTo?: string): Observable<BannerDetails> {
    // This can be used for testing (without calling API)
    // return of(this.defaultBannerDetails);

    // ToDo: probably, if dateFrom and dateTo are not specified, need to use default date or something else
    return this.http.get<BannerDetails>(`${this.url}/${departureAirportCode}/${arrivalAirportCode}/${language}/${dateFrom}/${dateTo}`)
      .pipe(catchError(this.handleError<BannerDetails>()));
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      // 'Proper' error handling/logging is needed
      console.log(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
