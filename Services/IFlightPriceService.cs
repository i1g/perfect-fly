﻿using System;
using PerfectFly.Models;

namespace PerfectFly.Services
{
    public interface IFlightPriceService
    {
        FlightPriceData GetBestTicketPrice(string departureAirportCode, string arrivalAirportCode, DateTime dateFrom, DateTime dateTo);
    }
}
