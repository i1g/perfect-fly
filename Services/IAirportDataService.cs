﻿using PerfectFly.Models;

namespace PerfectFly.Services
{
    public interface IAirportDataService
    {
        AirportData GetAirportData(string airportCode);
    }
}