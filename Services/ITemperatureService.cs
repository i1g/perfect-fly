﻿using System;
using PerfectFly.Models;

namespace PerfectFly.Services
{
    public interface ITemperatureService
    {
        TemperatureData GetAverageTemperature(string cityCode, DateTime dateFrom, DateTime dateTo);
    }
}
