﻿using System;
using Moq;
using Microsoft.AspNetCore.Mvc;
using PerfectFly.Models;
using PerfectFly.Services;
using System.Linq;

namespace PerfectFly.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BannerDetailsController : ControllerBase
    {
        //For test purposes (with mocked data) these lines are commented

        //private readonly IAirportDataService _airportDataService;
        //private readonly ITemperatureService _temperatureService;
        //private readonly IFlightPriceService _flightPriceService;

        //public BannerDetailsController(IAirportDataService airportDataService, ITemperatureService temperatureService, IFlightPriceService flightPriceService)
        //{
        //    _airportDataService = airportDataService;
        //    _temperatureService = temperatureService;
        //    _flightPriceService = flightPriceService;
        //}

        public BannerDetailsController()
        {
        }

        [HttpGet("{departureAirportCode}/{arrivalAirportCode}/{language}/{dateFrom}/{dateTo}")]
        public BannerDetails GetBannerDetails(string departureAirportCode, string arrivalAirportCode, string language, DateTime dateFrom, DateTime dateTo)
        {
            // Mock data just for example
            // Should be a proper implementation here

            // TODO: need to make dateFrom and dateTo optional and also take 'language' into consideration

            var airportDataServiceMock = new Mock<IAirportDataService>();
            var temperatureServiceMock = new Mock<ITemperatureService>();
            var flightPriceServiceMock = new Mock<IFlightPriceService>();
            var rnd = new Random();

            airportDataServiceMock.Setup(a => a.GetAirportData("OSL")).Returns(new AirportData
            {
                AirportName = "Oslo lufthavn",
                AirportСode = "OSL",
                CityName = "Oslo",
                CityСode = "OSL",
                CountryCode = "NO",
                CountryName = "Norway"
            });

            airportDataServiceMock.Setup(a => a.GetAirportData("JFK")).Returns(new AirportData
            {
                AirportName = "John F. Kennedy International Airport",
                AirportСode = "JFK",
                CityName = "New York City",
                CityСode = "NY",
                CountryCode = "US",
                CountryName = "USA"
            });

            var departureAirportData = airportDataServiceMock.Object.GetAirportData(departureAirportCode);
            var arrivalAirportData = airportDataServiceMock.Object.GetAirportData(arrivalAirportCode);

            temperatureServiceMock.Setup(t => t.GetAverageTemperature(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(new TemperatureData
            {
                AverageTemperature = rnd.Next(0, 50),
                IsCelsius = true
            });

            var temperatureData = temperatureServiceMock.Object.GetAverageTemperature(arrivalAirportData.CityСode, dateFrom, dateTo);

            flightPriceServiceMock.Setup(f => f.GetBestTicketPrice(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(new FlightPriceData
            {
                Price = rnd.Next(5, 555),
                Currency = "USD"
            });

            var flightPriceData = flightPriceServiceMock.Object.GetBestTicketPrice(departureAirportCode, arrivalAirportCode, dateFrom, dateTo);

            return new BannerDetails
            {
                DepartureAirportName = departureAirportData.AirportName, // Namings could be shortened/simplified
                ArrivalAirportName = arrivalAirportData.AirportName,
                ArrivalCityName = arrivalAirportData.CityName,
                MonthDisplayName = "June", // TODO: Need some logic to get month name from date periods
                Temperature = temperatureData.AverageTemperature,
                IsCelsius = temperatureData.IsCelsius,
                Price = flightPriceData.Price,
                Currency = flightPriceData.Currency
            };
        }
    }
}